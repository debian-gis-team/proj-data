#!/usr/bin/python3 -u

import argparse
import csv
import os
import sys

args = None


def csv2dep5():
    if args.verbose:
        print("Parsing input file: %s" % args.input_file)

    if not os.access(args.input_file, os.R_OK):
        print("Error: Cannot read input file: %s" % args.input_file)
        sys.exit(1)

    rows = []

    with open(args.input_file) as f:
        reader = csv.DictReader(f)

        for row in reader:
            if args.debug:
                print("Row: %s" % row)

            rows.append(row)

    license_shortname = {
        'Creative Commons Attribution 4.0':              'CC-BY-4.0',
        'Creative Commons Attribution-ShareAlike 4.0':   'CC-BY-SA-4.0',
        'Creative Commons CC0 1.0 Universal':            'CC0-1.0',
        'Creative Commons Zero':                         'CC0-1.0',
        (
          'Data licence Germany - attribution'
          ' - version 2.0'
        ):                                               'Data-license-Germany-attribution-2.0',  # noqa: E501
        (
          'Open Government Licence'
          ' - '
          'Canada'
          ' - '
          'http://'
          'open.canada.ca'
          '/en'
          '/open-government-licence-canada'
        ):                                               'OGL-Canada-2.0',
        (
          'Open License France - '
          'https://www.etalab.gouv.fr'
          '/wp-content/uploads'
          '/2014/05/Open_Licence.pdf'
        ):                                               'OGL-France-1.0',
        'Public domain':                                 'public-domain',
        'The 2-Clause BSD License':                      'BSD-2-Clause',
    }

    file_copyright = {}
    copyright_group = {}

    for row in rows:
        if (
            row['filename'].endswith('_README.txt') or
            row['filename'] == 'README.DATA' or
            row['version_removed'] != ''
        ):
            continue

        filename = row['filename']
        copyright = row['copyright']
        if 'license' in row:
            license = row['license']
        else:
            license = row['licence']

        if copyright == 'Disclaimed':
            copyright = copyright.lower()

        if license in license_shortname:
            license = license_shortname[license]
        elif license in license_shortname.values():
            pass
        else:
            license = "FIXME: %s" % license

        file_copyright[filename] = {
            'copyright': copyright,
            'license':   license,
        }

        if copyright not in copyright_group:
            copyright_group[copyright] = {}

        if license not in copyright_group[copyright]:
            copyright_group[copyright][license] = []

        copyright_group[copyright][license].append(filename)

    sections = [
       {
           'files':     ['*'],
           'copyright': 'disclaimed',
           'license':   'public-domain',
       },
    ]

    seen = {}

    for filename in file_copyright:
        copyright = file_copyright[filename]['copyright']
        license = file_copyright[filename]['license']

        if copyright not in seen or license not in seen[copyright]:
            section = {
                'files':     copyright_group[copyright][license],
                'copyright': copyright,
                'license':   license
            }

            sections.append(section)

            if copyright not in seen:
                seen[copyright] = {}

            seen[copyright][license] = True

    output = ''

    i = 0
    for section in sections:
        if i > 0:
            output += "\n"

        if section['license'] == 'public-domain':
            section['license'] += "\n This content is in the public domain."

        output += (
            "Files: %s\n"
            "Copyright: %s\n"
            "License: %s\n"
        ) % (
            "\n       ".join(section['files']),
            section['copyright'],
            section['license'],
        )

        i += 1

    print(output)


def main():
    global args

    default = {
        'input-file': 'copyright_and_licenses.csv',
    }

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-i', '--input-file',
        metavar='<PATH>',
        action='store',
        help='Path to input CSV file (default: %s)' % (
            default['input-file']
        ),
        default=default['input-file'],
    )
    parser.add_argument(
        '-d', '--debug',
        action='store_true',
        help='Enable debug output',
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='Enable verbose output',
    )

    args = parser.parse_args()

    if not args.input_file:
        print("Error: No input file specified!")
        sys.exit(1)

    csv2dep5()


if __name__ == "__main__":
    main()
